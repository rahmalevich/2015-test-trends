//
//  BTWLocationManager.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 15.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "LocationManager.h"

static CLLocationDirection const kDistanceFilter = 100.0f;

@interface LocationManager () <CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation LocationManager

#pragma mark - Initializaton
static LocationManager *_sharedInstance = nil;
+ (LocationManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [LocationManager new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = kDistanceFilter;
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
        {
            [self.locationManager requestAlwaysAuthorization];
        } else {
            [self initializeLocationManager];
        }
    }
    return self;
}

- (void)initializeLocationManager
{
    [self.locationManager startUpdatingLocation];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationManagerDidSetInitialLocationNotification object:nil];
}

#pragma mark - Location
- (void)updateLocation
{
    [_locationManager stopUpdatingLocation];
    [_locationManager startUpdatingLocation];
}

#pragma mark - Location manager delegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        [self initializeLocationManager];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.currentLocation = [locations firstObject];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationManagerDidUpdateLocationNotification object:nil];
}

#pragma mark - Geocoding
- (void)geocodeCurrentLocationWithCompletionHandler:(void (^)(NSDictionary *, NSError*))completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:_locationManager.location completionHandler:^(NSArray *placemarks, NSError *error){
            CLPlacemark *placemark = [placemarks firstObject];
            completionHandler(placemark.addressDictionary, error);
        }];
    }
}

@end