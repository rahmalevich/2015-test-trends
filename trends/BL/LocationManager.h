//
//  BTWLocationManager.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 15.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

static NSString *kLocationManagerDidSetInitialLocationNotification = @"kLocationManagerDidSetInitialLocationNotification";
static NSString * const kLocationManagerDidUpdateLocationNotification = @"kLocationManagerDidUpdateLocationNotification";

@interface LocationManager : NSObject

@property (nonatomic, strong, readonly) CLLocation *currentLocation;

+ (LocationManager *)sharedInstance;
- (void)updateLocation;
- (void)geocodeCurrentLocationWithCompletionHandler:(void (^)(NSDictionary *, NSError*))completionHandler;

@end
