//
//  TrendsManager.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

static NSString * const kTrendWasRemovedNotification = @"kTrendWasRemovedNotification";

@interface TrendsManager : NSObject

@property (nonatomic, assign, readonly) BOOL isSearchingTrends;

+ (TrendsManager *)sharedInstance;
- (void)addTrendWithTitle:(NSString *)trendTitle;
- (void)removeTrendWithTitle:(NSString *)trendTitle;
- (void)findTrendsForCurentLocationWithCompletionHandler:(TNetworkResponseBlock)completionHandler;

@end
