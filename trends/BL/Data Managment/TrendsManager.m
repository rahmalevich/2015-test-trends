//
//  TrendsManager.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "TrendsManager.h"
#import "NetworkHelper.h"
#import "ApiMethod+Trends.h"

@interface TrendsManager ()
@property (nonatomic, strong) NSManagedObjectContext *localContext;
@property (nonatomic, assign, readwrite) BOOL isSearchingTrends;
@end

@implementation TrendsManager

static TrendsManager *_sharedInstance = nil;
+ (TrendsManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [TrendsManager new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.localContext = [NSManagedObjectContext MR_context];
    }
    return self;
}

- (void)addTrendWithTitle:(NSString *)trendTitle
{
    if (trendTitle.length == 0) {
        return;
    }
    
    if (![Trend MR_findFirstByAttribute:@"title" withValue:trendTitle]) {
        Trend *newTrend = [Trend MR_createEntity];
        newTrend.title = trendTitle;
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}

- (void)removeTrendWithTitle:(NSString *)trendTitle
{
    [Trend MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"title = %@", trendTitle]];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kTrendWasRemovedNotification object:nil userInfo:@{@"title":trendTitle}];
}

- (void)findTrendsForCurentLocationWithCompletionHandler:(TNetworkResponseBlock)completionHandler
{
    if (_isSearchingTrends) {
        return;
    }
    self.isSearchingTrends = YES;
    
    [completionHandler copy];

    NSString *geoID = @"1"; // hardcoded worldwide location
    ApiMethod *method = [ApiMethod searchTrendsMethodByGeoID:geoID];
    
    NSMutableArray *trendsArray = [NSMutableArray array];
    [[NetworkHelper sharedInstance] sendRequestWithMethod:method backgroundProcessing:^(NSArray *response){
        [_localContext rollback];
        NSArray *fetchedTrendsArray = [[response firstObject] valueForKey:@"trends"];
        for (NSDictionary *trendDict in fetchedTrendsArray) {
            Trend *trend = [Trend MR_importFromObject:trendDict inContext:_localContext];
            [trendsArray addObject:trend];
        }
    } callback:^(NSArray *response, NSError *error){
        self.isSearchingTrends = NO;
        if (completionHandler) {
            completionHandler([NSArray arrayWithArray:trendsArray], error);
        }
    }];
}

@end
