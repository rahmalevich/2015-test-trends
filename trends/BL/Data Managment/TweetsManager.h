//
//  TweetsManager.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface TweetsManager : NSObject

@property (nonatomic, assign, readonly) BOOL isReloadingData;

+ (TweetsManager *)sharedInstance;
- (void)reloadTweetsWithCompletion:(TErrorBlock)completion;
- (void)loadNewerTweetsWithCompletion:(TErrorBlock)completion;
- (void)loadOlderTweetsWithCompletion:(TErrorBlock)completion;

@end
