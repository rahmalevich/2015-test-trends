//
//  TweetsManager.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "TweetsManager.h"
#import "TrendsManager.h"
#import "NetworkHelper.h"
#import "ApiMethod+Tweets.h"

@interface TweetsManager ()
@property (nonatomic, assign, readwrite) BOOL isReloadingData;
@end

@implementation TweetsManager

#pragma mark - Initialization
static TweetsManager *_sharedInstance = nil;
+ (TweetsManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [TweetsManager new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleRemovedTrend:) name:kTrendWasRemovedNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Utils
- (void)reloadTweetsWithCompletion:(TErrorBlock)completion
{
    self.isReloadingData = YES;
 
    [completion copy];
    __weak typeof(self) wself = self;
    [self loadTweetsWithCompletion:^(NSError *error){
        wself.isReloadingData = NO;
        if (completion) {
            completion(error);
        }
    } olderThen:nil newerThen:nil truncateExisting:YES];
}

- (void)loadNewerTweetsWithCompletion:(TErrorBlock)completion
{
    Tweet *newestTweet = [Tweet MR_findFirstOrderedByAttribute:@"date" ascending:NO];
    [self loadTweetsWithCompletion:completion olderThen:nil newerThen:newestTweet truncateExisting:NO];
}

- (void)loadOlderTweetsWithCompletion:(TErrorBlock)completion
{
    Tweet *oldestTweet = [Tweet MR_findFirstOrderedByAttribute:@"date" ascending:YES];
    [self loadTweetsWithCompletion:completion olderThen:nil newerThen:oldestTweet truncateExisting:NO];
}

- (void)loadTweetsWithCompletion:(TErrorBlock)completion olderThen:(Tweet *)maxTweet newerThen:(Tweet *)sinceTweet truncateExisting:(BOOL)truncateExisting
{
    [completion copy];
    
    if (truncateExisting) {
        [Tweet MR_truncateAll];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
    
    NSArray *trendsArray = [Trend MR_findAll];
    ApiMethod *method = [ApiMethod searchTweetsByTrends:trendsArray sinceID:sinceTweet.backend_id withMaxID:maxTweet.backend_id];
    [[NetworkHelper sharedInstance] sendRequestWithMethod:method backgroundProcessing:^(NSDictionary *response){
        NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
        NSArray *statusesArray = [response valueForKey:@"statuses"];
        for (NSDictionary *statusDict in statusesArray) {
            [Tweet MR_importFromObject:statusDict inContext:context];
        }
        [context MR_saveToPersistentStoreAndWait];
    } callback:^(NSDictionary *response, NSError *error){
        if (completion) {
            completion(error);
        }
    }];
}

- (void)handleRemovedTrend:(NSNotification *)notification
{
    // we have to truncate all cached tweets cause we cannot filter out
    // which one was fetched for which trend (or maybe I just haven't found way to do it)
    [self reloadTweetsWithCompletion:nil];
}

@end
