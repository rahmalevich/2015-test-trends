//
//  ApiMethod+Authorization.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ApiMethod.h"

@interface ApiMethod (Authorization)

+ (ApiMethod *)getBearerTokenMethod;

@end
