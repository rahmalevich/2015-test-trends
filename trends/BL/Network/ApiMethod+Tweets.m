//
//  ApiMethod+Tweets.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ApiMethod+Tweets.h"

@implementation ApiMethod (Tweets)

+ (ApiMethod *)searchTweetsByTrends:(NSArray *)trendsArray sinceID:(NSNumber *)sinceID withMaxID:(NSNumber *)maxID
{
    NSString *queryString = [[trendsArray valueForKey:@"queryString"] componentsJoinedByString:@"+OR+"];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"q":queryString}];
    if (sinceID) {
        params[@"since_id"] = [sinceID stringValue];
    }
    if (maxID) {
        params[@"max_id"] = [maxID stringValue];
    }
    return [self methodWithHTTPMethod:@"GET" URLString:@"1.1/search/tweets.json" params:params];
}

@end
