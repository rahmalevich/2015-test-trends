//
//  NetworkHelper.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "NetworkHelper.h"
#import "AuthorizationManager.h"
#import "ApiMethod.h"

#import "AFNetworking.h"

@interface NetworkHelper ()
@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;
@end

@implementation NetworkHelper

#pragma mark - Initialization
static NetworkHelper *_sharedInstance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [NetworkHelper new];
        _sharedInstance.manager = [AFHTTPRequestOperationManager new];
    });
    return _sharedInstance;
}

#pragma mark - Sending requests
//- (NSError *)APIErrorFromResponse:(NSDictionary *)response
//{
//    NSError *resultError = nil;    
//    NSInteger responseCode = [response[@"code"] integerValue];
//    if (!response.count > 0 || responseCode > 0) {
//        BTWApiErrorCode errorCode = responseCode > 0 ? responseCode : BTWApiErrorCodeUnknown;
//        resultError = [NSError errorWithDomain:kBTWAPIErrorDomain code:errorCode userInfo:nil];
//    }
//    return resultError;
//}

- (NSOperation *)operationWithMethod:(ApiMethod *)method callback:(TNetworkResponseBlock)callback
{
    return [self operationWithMethod:method backgroundProcessing:nil callback:callback useBasicAuthentification:NO];
}

- (NSOperation *)operationWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback
{
    return [self operationWithMethod:method backgroundProcessing:backgroundProcessingBlock callback:callback useBasicAuthentification:NO];
}

- (NSOperation *)operationWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback useBasicAuthentification:(BOOL)useBasicAuthentification
{
    [backgroundProcessingBlock copy];
    [callback copy];
    
    TNetworkResponseBlock internalCallback = ^(id response, NSError *error){
        if (!error) {
            if (backgroundProcessingBlock) { // background processing if necessary
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    backgroundProcessingBlock(response);
                    if (callback) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            callback(response, error);
                        });
                    }
                });
            } else { // or just dispatch callback if doesn't needed
                if (callback) {
                    callback(response, error);
                }
            }
        } else {
            if (callback) {
                callback(response, error);
            }
        }
    };
    
    NSMutableURLRequest *request = [_manager.requestSerializer requestWithMethod:method.httpMethod URLString:method.urlString parameters:method.params error:nil];
    if (useBasicAuthentification) {
        [request addValue:[self basicAuthorizationString] forHTTPHeaderField:@"Authorization"];
    } else {
        [request addValue:[NSString stringWithFormat:@"Bearer %@", [AuthorizationManager sharedInstance].bearerToken] forHTTPHeaderField:@"Authorization"];
    }
    
    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        internalCallback(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        internalCallback(nil, error);
    }];
    
    return operation;
}

- (void)enqueueOperation:(NSOperation *)operation
{
    AFHTTPRequestOperation *httpOperation = (AFHTTPRequestOperation *)operation;
    NSURLRequest *request = httpOperation.request;
    NSString *bodyString = request.HTTPBody ? [NSJSONSerialization JSONObjectWithData:request.HTTPBody options:NSJSONReadingAllowFragments error:nil] : @"";
    NSLog(@"*** SENDING REQUEST %@\n%@", [request.URL absoluteString], bodyString);
    
    [_manager.operationQueue addOperation:operation];
}

- (void)sendRequestWithMethod:(ApiMethod *)method callback:(TNetworkResponseBlock)callback
{
    [self sendRequestWithMethod:method backgroundProcessing:nil callback:callback useBasicAuthentification:NO];
}

- (void)sendRequestWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback
{
    [self sendRequestWithMethod:method backgroundProcessing:backgroundProcessingBlock callback:callback useBasicAuthentification:NO];
}

- (void)sendRequestWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback useBasicAuthentification:(BOOL)useBasicAuthentification
{
    AFHTTPRequestOperation *operation = (AFHTTPRequestOperation *)[self operationWithMethod:method backgroundProcessing:backgroundProcessingBlock callback:callback useBasicAuthentification:useBasicAuthentification];
    [self enqueueOperation:operation];
}

#pragma mark - Generating signatures
- (NSString *)basicAuthorizationString
{
    NSString *encodedConsumerToken = [kConsumerKey stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *encodedConsumerSecret = [kConsumerSecret stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *authorizationString = [encodedConsumerToken stringByAppendingFormat:@":%@", encodedConsumerSecret];
    NSData *authroizationStringData = [authorizationString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *resultString = [NSString stringWithFormat:@"Basic %@", [authroizationStringData base64EncodedStringWithOptions:0]];
    return resultString;
}

@end
