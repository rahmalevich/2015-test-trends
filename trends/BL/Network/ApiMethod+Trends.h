//
//  ApiMethod+Trends.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ApiMethod.h"

@interface ApiMethod (Trends)

+ (ApiMethod *)searchTrendsMethodByGeoID:(NSString *)geoID;

@end
