//
//  ApiMethod.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface ApiMethod : NSObject

@property (nonatomic, strong, readonly) NSString *httpMethod;
@property (nonatomic, strong, readonly) NSString *urlString;
@property (nonatomic, strong, readonly) NSDictionary *params;

+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params;
+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params encodeBody:(BOOL)encodeBody;

@end
