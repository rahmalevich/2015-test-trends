//
//  NetworkHelper.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

@class ApiMethod;
@interface NetworkHelper : NSObject

+ (instancetype)sharedInstance;

- (NSOperation *)operationWithMethod:(ApiMethod *)method callback:(TNetworkResponseBlock)callback;
- (NSOperation *)operationWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback;
- (NSOperation *)operationWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback useBasicAuthentification:(BOOL)useBasicAuthentification;
- (void)enqueueOperation:(NSOperation *)operation;

- (void)sendRequestWithMethod:(ApiMethod *)method callback:(TNetworkResponseBlock)callback;
- (void)sendRequestWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback;
- (void)sendRequestWithMethod:(ApiMethod *)method backgroundProcessing:(TDataBlock)backgroundProcessingBlock callback:(TNetworkResponseBlock)callback useBasicAuthentification:(BOOL)useBasicAuthentification;

@end
