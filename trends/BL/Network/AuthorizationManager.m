//
//  AuthorizationManager.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "AuthorizationManager.h"
#import "NetworkHelper.h"
#import "ApiMethod+Authorization.h"

static NSString * const kBearerTokenKey = @"kBearerTokenKey";

@interface AuthorizationManager ()
@property (nonatomic, copy, readwrite) NSString *bearerToken;
@end

@implementation AuthorizationManager

#pragma mark - Initialization
static AuthorizationManager *_sharedInstance = nil;
+ (AuthorizationManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [AuthorizationManager new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        _bearerToken = [[NSUserDefaults standardUserDefaults] objectForKey:kBearerTokenKey];
    }
    return self;
}

#pragma mark - Custom setters
- (void)setBearerToken:(NSString *)bearerToken
{
    _bearerToken = [bearerToken copy];
    
    if (bearerToken.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:bearerToken forKey:kBearerTokenKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBearerTokenKey];
    }
}

#pragma mark - Public methods
- (BOOL)isAuthorized
{
    return _bearerToken.length > 0;
}

- (void)authorizeWithCompletionHandler:(TErrorBlock)completionHandler
{
    if ([self isAuthorized]) { // we don't need to authorize once again
        if (completionHandler) {
            completionHandler(nil);
        }
    } else {
        [completionHandler copy];
        
        ApiMethod *method = [ApiMethod getBearerTokenMethod];
        [[NetworkHelper sharedInstance] sendRequestWithMethod:method backgroundProcessing:nil callback:^(NSDictionary * response, NSError *error)
        {
            if (!error) { // TODO: check response, generate error if there is no token
                self.bearerToken = [response valueForKey:@"access_token"];
            }
            if (completionHandler) {
                completionHandler(error);
            }
        } useBasicAuthentification:YES];
    }
}

@end
