//
//  ApiMethod.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ApiMethod.h"

@interface ApiMethod ()
@property (nonatomic, strong, readwrite) NSString *httpMethod;
@property (nonatomic, strong, readwrite) NSString *urlString;
@property (nonatomic, strong, readwrite) NSDictionary *params;
@end

@implementation ApiMethod

#pragma mark - Initialization
+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params
{
    return [self methodWithHTTPMethod:method URLString:urlString params:params encodeBody:YES];
}

+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params encodeBody:(BOOL)encodeBody
{
    ApiMethod *newMethod = [ApiMethod new];
    newMethod.httpMethod = method;
    newMethod.urlString = [kBaseURL stringByAppendingString:urlString];
    
    if (encodeBody) {
        NSMutableDictionary *encodedParams = [NSMutableDictionary dictionary];
        for (NSString *key in params.allKeys) {
            NSString *encodedKey = [key stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
            NSString *encodedVDalue = [params[key] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
            encodedParams[encodedKey] = encodedVDalue;
        }
        newMethod.params = [NSDictionary dictionaryWithDictionary:encodedParams];
    } else {
        newMethod.params = params;
    }
    
    return newMethod;
}

@end
