//
//  ApiMethod+Trends.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ApiMethod+Trends.h"

@implementation ApiMethod (Trends)

+ (ApiMethod *)searchTrendsMethodByGeoID:(NSString *)geoID
{
    NSDictionary *params = @{@"id":geoID};
    return [self methodWithHTTPMethod:@"GET" URLString:@"1.1/trends/place.json" params:params];
}

@end
