//
//  ApiMethod+Authorization.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ApiMethod+Authorization.h"

@implementation ApiMethod (Authorization)

+ (ApiMethod *)getBearerTokenMethod
{
    NSDictionary *paramsDictionary = @{@"grant_type":@"client_credentials"};
    return [self methodWithHTTPMethod:@"POST" URLString:@"oauth2/token" params:paramsDictionary encodeBody:NO];
}

@end
