//
//  ApiMethod+Tweets.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ApiMethod.h"

@interface ApiMethod (Tweets)

+ (ApiMethod *)searchTweetsByTrends:(NSArray *)trendsArray sinceID:(NSNumber *)sinceID withMaxID:(NSNumber *)maxID;

@end
