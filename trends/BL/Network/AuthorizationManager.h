//
//  AuthorizationManager.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface AuthorizationManager : NSObject

@property (nonatomic, copy, readonly) NSString *bearerToken;

+ (AuthorizationManager *)sharedInstance;
- (BOOL)isAuthorized;
- (void)authorizeWithCompletionHandler:(TErrorBlock)completionHandler;

@end
