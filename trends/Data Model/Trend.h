//
//  Trend.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Trend : NSManagedObject

- (NSString *)queryString;

@end

NS_ASSUME_NONNULL_END

#import "Trend+CoreDataProperties.h"
