//
//  Trend.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "Trend.h"

@implementation Trend

- (NSString *)queryString
{
    NSString *titleString = [self.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([titleString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].count > 1) {
        titleString = [NSString stringWithFormat:@"\"%@\"", titleString];
    }
    NSString *resultString = [titleString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return resultString;
}

@end
