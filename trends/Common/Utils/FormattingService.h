//
//  FormattingService.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface FormattingService : NSObject

+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format;
+ (NSDateFormatter *)shortDateTimeFormatter;

@end
