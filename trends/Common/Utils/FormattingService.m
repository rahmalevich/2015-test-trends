//
//  FormattingService.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "FormattingService.h"

@implementation FormattingService

#pragma mark - Caching
NSMutableDictionary *_sharedDictionary = nil;
+ (NSMutableDictionary *)sharedFormattersDicitionary
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDictionary = [NSMutableDictionary dictionary];
    });
    return _sharedDictionary;
}

+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format
{
    NSDateFormatter *resultFormatter = [self sharedFormattersDicitionary][format];
    if (!resultFormatter) {
        resultFormatter = [[NSDateFormatter alloc] init];
        resultFormatter.dateFormat = format;
        [self sharedFormattersDicitionary][format] = resultFormatter;
    }
    return resultFormatter;
}

#pragma mark - Public
+ (NSDateFormatter *)shortDateTimeFormatter
{
    static NSString *kShortDateTimeFormatterKey = @"kShortDateTimeFormatterKey";
    NSDateFormatter *formatter = [self sharedFormattersDicitionary][kShortDateTimeFormatterKey];
    if (!formatter) {
        formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterMediumStyle;
        formatter.timeStyle = NSDateFormatterShortStyle;
        formatter.doesRelativeDateFormatting = YES;
        [self sharedFormattersDicitionary][kShortDateTimeFormatterKey] = formatter;
    }
    return formatter;
}

@end
