//
//  Blocks.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

@class NSError;

typedef void(^TNetworkResponseBlock)(id response, NSError *error);
typedef id(^TResponseParsingBlock)(id response, NSError **error);

typedef void(^TVoidBlock)(void);
typedef void(^TDataBlock)(id);
typedef void(^TErrorBlock)(NSError *);
