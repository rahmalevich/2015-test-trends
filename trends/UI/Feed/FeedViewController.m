//
//  FeedViewController.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "FeedViewController.h"
#import "FeedCell.h"

#import "AuthorizationManager.h"
#import "TweetsManager.h"

#import "MBProgressHUD.h"
#import "UIScrollView+BottomRefreshControl.h"

@interface FeedViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIRefreshControl *topRefreshControl;
@property (nonatomic, strong) UIRefreshControl *bottomRefreshControl;
@property (nonatomic, strong) NSFetchedResultsController *tweetsDataController;
@end

@implementation FeedViewController

#pragma mark - Initialization
- (void)dealloc
{
    [[TweetsManager sharedInstance] removeObserver:self forKeyPath:@"isReloadingData"];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *filterItem = [[UIBarButtonItem alloc] initWithTitle:@"Filter" style:UIBarButtonItemStylePlain target:self action:@selector(showFilter:)];
    self.navigationItem.rightBarButtonItem = filterItem;
    
    // refresh controls
    self.topRefreshControl = [UIRefreshControl new];
    [_topRefreshControl addTarget:self action:@selector(handleTopRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_topRefreshControl];
    
    self.bottomRefreshControl = [UIRefreshControl new];
    [_bottomRefreshControl addTarget:self action:@selector(handleBottomRefresh:) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = _bottomRefreshControl;
    
    self.tweetsDataController = [Tweet MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"date" ascending:NO delegate:self];
    [self updateAppearence];
    
    [[TweetsManager sharedInstance] addObserver:self forKeyPath:@"isReloadingData" options:NSKeyValueObservingOptionNew context:nil];
    
    // handling authorization status
    if (![[AuthorizationManager sharedInstance] isAuthorized]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Authorization...";
        [[AuthorizationManager sharedInstance] authorizeWithCompletionHandler:^(NSError *error){
            [[TweetsManager sharedInstance] reloadTweetsWithCompletion:^(NSError *error){
                [hud hide:YES];
            }];
        }];
    } else {
        [[TweetsManager sharedInstance] reloadTweetsWithCompletion:nil];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.tableView reloadData];
}

- (void)updateAppearence
{
    if (![[AuthorizationManager sharedInstance] isAuthorized]) {
        _tableView.alpha = 0.0f;
        _placeholderLabel.alpha = 0.0f;
        [_activityIndicator stopAnimating];
        return;
    }
    
    if ([_tweetsDataController.fetchedObjects count] == 0) {
        if ([TweetsManager sharedInstance].isReloadingData) {
            [_activityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
                _tableView.alpha = 0.0f;
            }];
        } else {
            [_activityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0f;
                _tableView.alpha = 0.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            _tableView.alpha = 1.0f;
            _placeholderLabel.alpha = 0.0f;
        }];
    }
}

#pragma mark - Utils
- (void)showFilter:(id)sender
{
    [self performSegueWithIdentifier:@"ShowFilterSegue" sender:nil];
}

- (void)handleTopRefresh:(id)sender
{
    [[TweetsManager sharedInstance] loadNewerTweetsWithCompletion:^(NSError *error){
        [_topRefreshControl endRefreshing];
    }];
}

- (void)handleBottomRefresh:(id)sender
{
    [[TweetsManager sharedInstance] loadOlderTweetsWithCompletion:^(NSError *error){
        [_bottomRefreshControl endRefreshing];
    }];
}

#pragma mark - Observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"isReloadingData"]) {
        [self updateAppearence];
    }
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tweet *tweet = _tweetsDataController.fetchedObjects[indexPath.row];
    return [FeedCell cellHeightForTweet:tweet tableWidth:CGRectGetWidth(tableView.frame)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tweetsDataController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FeedCellID" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(FeedCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Tweet *tweet = _tweetsDataController.fetchedObjects[indexPath.row];
    cell.tweet = tweet;
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
