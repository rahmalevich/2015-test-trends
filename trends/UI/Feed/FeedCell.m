//
//  FeedCell.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "FeedCell.h"
#import "FormattingService.h"

static CGFloat const kPadding = 10.0;
static CGFloat const kTextOffset = 55.0;

@interface FeedCell ()
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *nicknameLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UITextView *bodyTextView;
@end

@implementation FeedCell

+ (CGFloat)cellHeightForTweet:(Tweet *)tweet tableWidth:(CGFloat)tableWidth
{
    CGFloat textHeight = [tweet.text boundingRectWithSize:(CGSize){tableWidth - 2 * kPadding, 1000.0f} options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size.height;
    CGFloat resultHeight = kTextOffset + textHeight + kPadding;
    return resultHeight;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _bodyTextView.textContainerInset = UIEdgeInsetsZero;
}

- (void)setTweet:(Tweet *)tweet
{
    _tweet = tweet;

    _nameLabel.text = tweet.author_name;
    _nicknameLabel.text = tweet.author_nickname;
    _bodyTextView.text = tweet.text;
    _dateLabel.text = [[FormattingService shortDateTimeFormatter] stringFromDate:tweet.date];
}

@end
