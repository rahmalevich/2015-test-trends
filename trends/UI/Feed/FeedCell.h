//
//  FeedCell.h
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

@interface FeedCell : UITableViewCell

@property (nonatomic, strong) Tweet *tweet;

+ (CGFloat)cellHeightForTweet:(Tweet *)tweet tableWidth:(CGFloat)tableWidth;
@end
