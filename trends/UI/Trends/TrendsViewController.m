//
//  TrendsViewController.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "TrendsViewController.h"
#import "TrendsManager.h"

static CGFloat const kTrendCellHeight = 44.0;

@interface TrendsViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UITextField *addTrendField;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, strong) NSFetchedResultsController *trendsDataController;
@end

@implementation TrendsViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.trendsDataController = [Trend MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"title" ascending:YES delegate:self];
    [self.tableView reloadData];
    
    [self updateAppearence];
}

- (void)updateAppearence
{
    if ([_trendsDataController.fetchedObjects count] == 0) {
        [UIView animateWithDuration:0.3 animations:^{
            _placeholderLabel.alpha = 1.0f;
            _tableView.alpha = 0.0f;
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            _tableView.alpha = 1.0f;
            _placeholderLabel.alpha = 0.0f;
        }];
    }
}

#pragma mark - UITableView delegagate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _trendsDataController.fetchedObjects.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTrendCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *trendCellID = @"trendCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:trendCellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:trendCellID];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Trend *trend = _trendsDataController.fetchedObjects[indexPath.row];
    cell.textLabel.text = trend.title;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Trend *trend = _trendsDataController.fetchedObjects[indexPath.row];
        [[TrendsManager sharedInstance] removeTrendWithTitle:trend.title];
    }
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    [self updateAppearence];
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self addTrend];
    [textField resignFirstResponder];
    return YES;
}

- (void)addTrend
{
    if (_addTrendField.text.length > 0) {
        [[TrendsManager sharedInstance] addTrendWithTitle:_addTrendField.text];
    }
}

#pragma mark - Actions
- (IBAction)actionAddTrend:(id)sender
{
    [self addTrend];
}

@end
