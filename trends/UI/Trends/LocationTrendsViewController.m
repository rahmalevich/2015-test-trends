//
//  LocationTrendsViewController.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "LocationTrendsViewController.h"
#import "TrendsManager.h"

static CGFloat const kTrendCellHeight = 44.0;

@interface LocationTrendsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSArray *tableModel;
@property (nonatomic, strong) NSFetchedResultsController *trendsDataController;
@end

@implementation LocationTrendsViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateAppearence];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.trendsDataController = [Trend MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"title" ascending:YES];
    
    [[TrendsManager sharedInstance] findTrendsForCurentLocationWithCompletionHandler:^(NSArray *response, NSError *error)
    {
        self.tableModel = response;
        [_tableView reloadData];
        [self updateAppearence];
    }];
}

- (void)updateAppearence
{
    if (_tableModel.count == 0) {
        if ([TrendsManager sharedInstance].isSearchingTrends) {
            [_activityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
                _tableView.alpha = 0.0f;
            }];
        } else {
            [_activityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0f;
                _tableView.alpha = 0.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            _tableView.alpha = 1.0f;
            _placeholderLabel.alpha = 0.0f;
        }];
    }
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTrendCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *trendCellID = @"trendCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:trendCellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:trendCellID];
    }
    Trend *trend = _tableModel[indexPath.row];
    cell.textLabel.text = trend.title;
    
    // not the best decision but don't have time to fix :(
    BOOL trendSelected = [self.trendsDataController.fetchedObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"title = %@", trend.title]].count > 0;
    cell.accessoryType = trendSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    Trend *trend = _tableModel[indexPath.row];
    BOOL trendSelected = [self.trendsDataController.fetchedObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"title = %@", trend.title]].count > 0;
    if (trendSelected) {
        [[TrendsManager sharedInstance] removeTrendWithTitle:trend.title];
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        [[TrendsManager sharedInstance] addTrendWithTitle:trend.title];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

@end
