//
//  FilterContainerViewController.m
//  trends
//
//  Created by Mikhail Rakhmalevich on 24.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "FilterContainerViewController.h"
#import "Masonry.h"

@interface FilterContainerViewController ()
@property (nonatomic, weak) IBOutlet UIView *contentContainer;
@property (nonatomic, strong) UIViewController *trendsViewController;
@property (nonatomic, strong) UIViewController *locationTrendsViewController;
@property (nonatomic, strong) UIPageViewController *pageController;
@end

@implementation FilterContainerViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.trendsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TrendsViewController"];
    self.locationTrendsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationTrendsViewController"];

    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:0];
    [_pageController setViewControllers:@[_trendsViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];

    [self addChildViewController:self.pageController];
    [self.contentContainer addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
    
    [self.pageController.view mas_makeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self.pageController.view.superview);
    }];
}

#pragma mark - Actions
- (IBAction)actionSegmentValueChanged:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0) {
        [_pageController setViewControllers:@[_trendsViewController] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    } else {
        [_pageController setViewControllers:@[_locationTrendsViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
}

- (IBAction)actionClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
